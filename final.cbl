       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL.


       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT TRADER-9 ASSIGN TO "trader9.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  TRADER-9.
       01  TRADER.
           88 END-OF-TRADER-FILE VALUE HIGH-VALUES.
           05 ID-PROVINCE        PIC 9(2).
           05 ID-TRADER          PIC 9(4).
           05 INCOME             PIC 9(6).

       WORKING-STORAGE SECTION. 
       



       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT TRADER-9
           READ TRADER-9
              AT END SET END-OF-TRADER-FILE TO TRUE
           END-READ
           PERFORM UNTIL END-OF-TRADER-FILE
              DISPLAY TRADER
              READ TRADER-9
                AT END SET END-OF-TRADER-FILE TO TRUE
               END-READ
           END-PERFORM
           
           CLOSE TRADER-9 
           GOBACK .
